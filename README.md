# Docker .cache/pip/wheels 

### arm32v7.cache.pip.tar.gz 

- git branch 'arm32v7/pytorch/ml/v1.4'

- Docker technep/arm32v7-pytorch-ml

- Dependencies 

```bash
 apt-get install -yq --no-install-recommends \
      gfortran \
      libatlas-dev \
      libblas-dev \
      liblapack-dev \
      protobuf-compiler \
      libprotoc-dev 
```


```bash

./.cache/pip/wheels/32/fa/e2/13fbb3561f7ac73c664b6ab01c0c99a6cae64f8f218a88ec6b/PyWavelets-1.1.1-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/e2/46/78/e78f76c356bca9277368f1f97a31b37a8cb937176d9511af31/mpmath-1.1.0-py3-none-any.whl
./.cache/pip/wheels/36/93/24/29e375ee74e0f178889e6906cb73e693e9f06a5f589dcee6b9/watchdog-0.10.2-py3-none-any.whl
./.cache/pip/wheels/cf/01/90/7c26dc640e85f827af3bd91186a5deed2f36b44927d8277dbd/matplotlib-3.1.3-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/ac/5c/3a/a80e1c65880945c71fd833408cd1e9a8cb7e2f8f37620bb75b/torchfile-0.1.0-py3-none-any.whl
./.cache/pip/wheels/ba/ad/c8/2d98360791161cd3db6daf6b5e730f34021fc9367d5879f497/terminaltables-3.1.0-py3-none-any.whl
./.cache/pip/wheels/6b/fd/8c/a20dd591c1a554070cc33fb58042867e6ac1c85395abe2e57a/graphql_core-1.1-py3-none-any.whl
./.cache/pip/wheels/b9/d9/ae/63bf9056b0a22b13ade9f6b9e08187c1bb71c47ef21a8c9924/MarkupSafe-1.1.1-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/29/93/c6/762e359f8cb6a5b69c72235d798804cae523bbe41c2aa8333d/promise-2.3-py3-none-any.whl
./.cache/pip/wheels/71/c8/8c/96516845b3a3b294ac0454313706eb424f384a7c76d438dd57/pyzmq-19.0.0-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/0a/e4/17/6553eef9223d4266539532502327af0aae43f46ff5d392d361/numpy-1.14.5-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/7e/9b/a8/99d57c4242145217502b9fd24befa29c99b5bf09921c4eba8e/scikit_image-0.16.2-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/f9/8d/8d/f6af3f7f9eea3553bc2fe6d53e4b287dad18b06a861ac56ddf/retrying-1.3.3-py3-none-any.whl
./.cache/pip/wheels/ff/bd/f9/ac5a9a019da5be2072e8a283f36d28b219762cebdd21ff3c68/scipy-1.4.1-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/63/99/01/9fe785b86d1e091a6b2a61e06ddb3d8eb1bc9acae5933d4740/pandocfilters-1.4.2-py3-none-any.whl
./.cache/pip/wheels/46/ef/c3/157e41f5ee1372d1be90b09f74f82b10e391eaacca8f22d33e/sklearn-0.0-py2.py3-none-any.whl
./.cache/pip/wheels/b6/9a/56/5456fd32264a8fc53eefcb2f74e24e99a7ef4eb40a9af5c905/gql-0.2.0-py3-none-any.whl
./.cache/pip/wheels/b6/e7/50/aee9cc966163d74430f13f208171dee22f11efa4a4a826661c/psutil-5.7.0-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/9e/56/4f/da13e448a8a5b8671b2954600d5355cf36e557c7aa5020139b/backcall-0.1.0-py3-none-any.whl
./.cache/pip/wheels/fe/23/b9/c484fd88abbb1afb8da65ad089ea0a5a126e2d9bd17832a0ca/onnx-1.6.0-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/d0/31/2c/9406ed59f0dcdce0c453a8664124d738551590e74fc087f604/tornado-6.0.3-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/30/1b/01/b54cb30133b382c3450b83c57940a8a8a13f62054d5a3b12d0/shortuuid-0.5.0-py3-none-any.whl
./.cache/pip/wheels/30/0c/26/59ba285bf65dc79d195e9b25e2ddde4c61070422729b0cd914/prometheus_client-0.7.1-py3-none-any.whl
./.cache/pip/wheels/50/ca/fa/8fca8d246e64f19488d07567547ddec8eb084e8c0d7a59226a/subprocess32-3.5.4-py3-none-any.whl
./.cache/pip/wheels/5d/65/d3/95b9e6aa6fca468264327f67a8e440bc20722a745dfb043d76/kiwisolver-1.1.0-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/57/74/e3/61db397ec89f304e49711ec9f68490f15814b80c1c0ee9b8c0/pyrsistent-0.15.7-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/d9/69/11/ef91c3164ec6433cc400c6972bb97528f2b1fd59002f37bf33/pandas-1.0.1-cp37-cp37m-linux_armv7l.whl
./.cache/pip/wheels/56/b0/fe/4410d17b32f1f0c3cf54cdfb2bc04d7b4b8f4ae377e2229ba0/future-0.18.2-py3-none-any.whl
./.cache/pip/wheels/2d/d1/9b/cde923274eac9cbb6ff0d8c7c72fe30a3da9095a38fd50bbf1/visdom-0.1.8.9-py3-none-any.whl
./.cache/pip/wheels/3e/31/09/fa59cef12cdcfecc627b3d24273699f390e71828921b2cbba2/pathtools-0.1.2-py3-none-any.whl
./.cache/pip/wheels/df/99/da/c34f202dc8fd1dffd35e0ecf1a7d7f8374ca05fbcbaf974b83/nvidia_ml_py3-7.352.0-py3-none-any.whl
./.cache/pip/wheels/a9/f0/08/f43775ff479fddfcd7f3ef76d3e015ae927dca638a086c3dda/scikit_learn-0.22.1-cp37-cp37m-linux_armv7l.whl

```
